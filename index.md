# Stylo
## L'éditeur de texte sémantique pour l'édition scientifique
<br />
<small>Chaire de recherche du Canada sur les écritures numériques<br /> <a href="https://ecrituresnumeriques.ca">ecrituresnumeriques.ca</a></small>

<img src="images/logo.svg" class="logo"/> <img src="images/logo-udem.png" class="logo2"/>

===d===
## Pourquoi Stylo ?

- écrire **avec** le numérique

<!-- .element: class="fragment" -->
- maîtriser la structure du texte<!-- .element: class="fragment" -->
- simplifier et pérenniser la gestion bibliographique<!-- .element: class="fragment" -->

===b===
## Traitement de texte ?

- Word est un logiciel propriétaire qui vous enferme<!-- .element: class="fragment" -->
- confusion entre structure et mise en forme<!-- .element: class="fragment" -->
- lourdeur d'utilisation<!-- .element: class="fragment" -->

===b===

<!-- .slide: data-background="images/word.png" -->

===b===
> Word est un instrument de douleur pour vous et pour le reste du monde.

===b===
## Se passer de traitement de texte ?

- c'est possible !<!-- .element: class="fragment" -->
- privilégier un format interopérable<!-- .element: class="fragment" -->
- le texte brut et un langage de balisage léger<!-- .element: class="fragment" -->

===b===
## Markdown

- langage de balisage léger créé en 2014 par John Gruber et Aaron Swartz<!-- .element: class="fragment" -->
- objectif : un langage simple pour les humains compréhensible aussi par les machines<!-- .element: class="fragment" -->
- une dizaine de balises pour structurer du texte<!-- .element: class="fragment" -->

===b===
## Prise en main de Markdown

En 15 minutes :  
[https://frama.link/markdown](https://artorig.github.io/tutomd/tutorial/)

===d===
## Stylo est pensé pour la rédaction et l'édition scientifique

===b===
## Fonctionnement de stylo

- se concentrer sur le contenu et sa structure (avec Markdown)<!-- .element: class="fragment" -->
- séparer le texte, les métadonnées et la bibliographie<!-- .element: class="fragment" -->
- produire différentes formes de votre document (page web annotable, PDF, docx, XML)<!-- .element: class="fragment" -->

===b===

<!-- .slide: data-background="images/stylo-nouvelles-vues.png" -->

===b===
## Interface de Stylo

- espace central : texte (Markdown)<!-- .element: class="fragment" -->
- colonne de gauche : versions, table des matières, bibliographie<!-- .element: class="fragment" -->
- colonne de droite : métadonnées<!-- .element: class="fragment" -->

===b===

<!-- .slide: data-background="images/stylo-versions.png" -->

===n===
Versions.

===b===

<!-- .slide: data-background="images/stylo-toc.png" -->

===n===
Sommaire.

===b===

<!-- .slide: data-background="images/stylo-bibliography.png" -->

===n===
Bibliographie.

===b===

<!-- .slide: data-background="images/stylo-editor-mode.png" -->

===n===
Métadonnées.

===b===

<!-- .slide: data-background="images/stylo-raw-mode.png" -->

===d===
## Prise en main de Stylo !

### [stylo.ecrituresnumeriques.ca](https://stylo.ecrituresnumeriques.ca/)

===d===

## Contact
<small>Antoine Fauchié – [antoine.fauchie@umontreal.ca](mailto:antoine.fauchie@umontreal.ca)  
Chaire de recherche du Canada sur les écritures numériques – [ecrituresnumeriques.ca](https://ecrituresnumeriques.ca/)</small>

[stylo.ecrituresnumeriques.ca](https://stylo.ecrituresnumeriques.ca/)
